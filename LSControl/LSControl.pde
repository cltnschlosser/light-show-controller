/**
  * This sketch demonstrates how to use the BeatDetect object in FREQ_ENERGY mode.<br />
  * You can use <code>isKick</code>, <code>isSnare</code>, </code>isHat</code>, <code>isRange</code>, 
  * and <code>isOnset(int)</code> to track whatever kind of beats you are looking to track, they will report 
  * true or false based on the state of the analysis. To "tick" the analysis you must call <code>detect</code> 
  * with successive buffers of audio. You can do this inside of <code>draw</code>, but you are likely to miss some 
  * audio buffers if you do this. The sketch implements an <code>AudioListener</code> called <code>BeatListener</code> 
  * so that it can call <code>detect</code> on every buffer of audio processed by the system without repeating a buffer 
  * or missing one.
  * <p>
  * This sketch plays an entire song so it may be a little slow to load.
  */

import processing.serial.*;

import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.spi.*;

Minim minim;

AudioPlayer song;
AudioSource input;

BeatDetect beat;
BeatListener bl;

Serial port;

final int HAT_PIN =  12;
final int SNARE_PIN =  8;
final int KICK_PIN =  7;

final int WIN_WIDTH = 512;
final int WIN_HEIGHT = 200;
final String WIN_TITLE = "Light Show Controller";
final String FONT_NAME = "Helvetica";
final int TEXT_SIZE_MIN = 16;
final int TEXT_SIZE_MAX = 32;

final int BUFFER_SIZE = 2048;
final int BIT_RATE = 48000;
final int BIT_DEPTH = 16;
final int SERIAL_SPEED = 57600;
final int BEAT_SENSITIVITY = 100;

boolean arduino_connected = true;

float kickSize = TEXT_SIZE_MIN;
float snareSize = TEXT_SIZE_MIN;
float hatSize = TEXT_SIZE_MIN;

int slider_pos = 100;
String song_name = "";
boolean repeat = false;
boolean draggingSlider = false;
boolean load_file_hover = false;
boolean load_playlist_hover = false;
boolean line_in_hover = false;
boolean streaming = false;

boolean playlist_mode = false;
ArrayList<String> playlist;
int playlist_index;

void setup() {
  //WINDOW SETUP
  frame.setTitle(WIN_TITLE);
  size(WIN_WIDTH, WIN_HEIGHT, P3D);
  textFont(createFont(FONT_NAME, TEXT_SIZE_MIN));
  
  setupSerial();
  
  minim = new Minim(this);
}

void stop() {
  // always close Minim audio classes when you are finished with them
  closeSong();
  closeStream();
  // always stop Minim before exiting
  minim.stop();
  // this closes the sketch
  super.stop();
}

void dispose()
{
  stop();
  
  super.dispose();
}

void draw()
{
  updatePlaylist();
  
  background(0);
  fill(255);
  stroke(255);
  
  if(beat != null)
  {
    if(beat.isKick()) {
        sendSerial(KICK_PIN);
        kickSize = TEXT_SIZE_MAX;
    }
    if(beat.isSnare()) {
        sendSerial(SNARE_PIN);
        snareSize = TEXT_SIZE_MAX;
    }
    if(beat.isHat()) {
        sendSerial(HAT_PIN);
        hatSize = TEXT_SIZE_MAX;
    }
  }
  
  textAlign(CENTER);
  textSize(kickSize);
  text("KICK", width/4, height/2);
  
  textSize(snareSize);
  text("SNARE", width/2, height/2);
  
  textSize(hatSize);
  text("HAT", 3*width/4, height/2);
  textAlign(LEFT);
  
  if(streaming || songPlaying())//Dont change text size when paused
  {
    kickSize = constrain(kickSize * 0.95, TEXT_SIZE_MIN, TEXT_SIZE_MAX);
    snareSize = constrain(snareSize * 0.95, TEXT_SIZE_MIN, TEXT_SIZE_MAX);
    hatSize = constrain(hatSize * 0.95, TEXT_SIZE_MIN, TEXT_SIZE_MAX);
  }
  
  if(!streaming)
  {
    //Song Position (Slider)
    slider_pos = song != null ? (int) map(songPos(), 0, songLength(), 100, 400) : 100;
    line(100, 150, 400, 150);
    line(slider_pos, 140, slider_pos, 160);
  
    //Time
    textSize(14);
    text(formatTime(songPos()) + " / " + formatTime(songLength()), 405, 155);
  
    //Play/Pause
    if(songPlaying())
    {
      line(81, 143, 81, 157);
      line(89, 143, 89, 157);
    }
    else
    {
      triangle(82, 142, 90, 150, 82, 158);
    }
  }
  //Load file Button
  textSize(16);
  if(load_file_hover)
  { 
    rect(4, 181, 68, 16, 7);//x, y, width, height
    
    fill(0);
    text("Load File", 5, 195);
    fill(255);
  }
  else
  {
    text("Load File", 5, 195);
  }
  
  //Load playlist
  if(load_playlist_hover)
  { 
    rect(78, 181, 92, 16, 7);//x, y, width, height
    
    fill(0);
    text("Load Playlist", 79, 195);
    fill(255);
  }
  else
  {
    text("Load Playlist", 79, 195);
  }
  
  //Line In
  if(line_in_hover)
  { 
    rect(4, 161, 61, 16, 7);//x, y, width, height
    
    fill(0);
    text("Line In", 5, 175);
    fill(255);
  }
  else
  {
    text("Line In", 5, 175);
  }
  
  //Now Playing
  text("Now playing: " + song_name, 200, 195);
  
  //Visualizer
  if(streaming)
  {
    drawWaveform(input);
  }
  else
  {
    drawWaveform(song);
  }
}

void mouseMoved()
{
  //Load file
  if(mouseX >= 4 && mouseX <= 72 &&
     mouseY >= 181 && mouseY <= 197)
  {
    load_file_hover = true;
  }
  else
  {
    load_file_hover = false;
  }
  
  //Load playlist
  if(mouseX >= 78 && mouseX <= 170 &&
     mouseY >= 181 && mouseY <= 197)
  {
    load_playlist_hover = true;
  }
  else
  {
    load_playlist_hover = false;
  }
  
  //Line In
  if(mouseX >= 4 && mouseX <= 65 &&
     mouseY >= 161 && mouseY <= 177)
  {
    line_in_hover = true;
  }
  else
  {
    line_in_hover = false;
  }
}

void mouseClicked()
{
  //Play/Pause START
  if(mouseX >= 80 && mouseX <= 90 &&
     mouseY >= 140 && mouseY <= 160)
  {
    playpause();
  }
  //Play/Pause END
  
  if(load_file_hover)
  {
    selectInput("Choose a file to play", "selectFile");
  }
  
  if(load_playlist_hover)
  {
    selectFolder("Choose a folder to play", "selectPlaylist");
  }
  
  if(line_in_hover)
  {
    loadFromSpeakers();
  }
}

void mousePressed()
{
  if(mouseX >= slider_pos-3 && mouseX <= slider_pos+3 &&
     mouseY >= 140 && mouseY <= 160 && song != null)
  {
    draggingSlider = true;
  }
}

void mouseDragged()
{
  if(draggingSlider)
  {
    int song_pos = (int) map(constrain(mouseX, 100, 400), 100, 400, 0, song.length());
    song.cue(song_pos);
  }
}

void mouseReleased()
{
  draggingSlider = false;
}

void keyPressed()
{
  if(key == CODED)
  {
    
  }
  else
  {
    switch(key)
    {
      case ' ':
        playpause();
        break;
    }
  }
}

void updatePlaylist()
{
  if(playlist_mode)
  {
    if(songFinished() && playlist_index < playlist.size()-1)
    {
      playlist_index++;
      loadSong(playlist.get(playlist_index));
    }
  }
}

void selectFile(File file)
{
  song_name = "";
  
  if (file == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    String path = file.getAbsolutePath();
    if(path.endsWith(".mp3") || path.endsWith(".wav"))
    {
      playlist_mode = false;
      loadSong(path);
    }
    else
    {
      println("Invalid Extension");
    }
    println("Loading: " + path);
  }
}

void selectPlaylist(File folder)
{
  song_name = "";
  
  if (folder == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    playlist = new ArrayList<String>();
    for (final File file : folder.listFiles()) {
      String path = file.getAbsolutePath();
      if(path.endsWith(".mp3") || path.endsWith(".wav"))
      {
        playlist.add(path);
      }
    }
    if(playlist.size() > 0)
    {
      playlist_mode = true;
      playlist_index = 0;
      loadSong(playlist.get(playlist_index));
    }
  }
  
}

void playpause()
{
  if(song != null)
  {
    if(songFinished())
    {
      song.play(0);
    }
    else
    {
      if(song.isPlaying())
      {
        song.pause();
        //noLoop();
      }
      else
      {
        playSong();
        //loop();
      }
    }
  }
}

void drawWaveform(AudioSource source)
{
  if(source != null)
  {
    for(int i = 0; i < source.bufferSize() - 1; i++)
    {
      float x1 = map( i, 0, source.bufferSize(), 0, width );
      float x2 = map( i+1, 0, source.bufferSize(), 0, width );
      line( x1, 40 + source.mix.get(i)*35, x2, 40 + source.mix.get(i+1)*35 );
    }
  }
  else
  {
    line(0, 40, width, 40);
  }
}

void loadSong(String filename)
{
  streaming = false;
  
  closeSong();
  
  song_name = filename.substring(filename.lastIndexOf('\\')+1, filename.length() - 4);
  if(playlist_mode)
  {
    song_name += " (" + (playlist_index+1) + "/" + playlist.size() + ")";
  }
  
  song = minim.loadFile(filename, BUFFER_SIZE);
  
  createBeatDetector(song);
  
  playSong();
}

void loadFromSpeakers()//TODO track down error that occurs ocasionally - may only be when stereo out isn't playing anything
{
  closeSong();
  song = null;
  
  streaming = true;
  
  if(input == null)
  {
    input = minim.getLineIn();
  }
  
  song_name = "LINE_IN";
  
  createBeatDetector(input);
}

void createBeatDetector(AudioSource source)
{
  // a beat detection object that is FREQ_ENERGY mode that 
  // expects buffers the length of song's buffer size
  // and samples captured at songs's sample rate
  beat = new BeatDetect(source.bufferSize(), source.sampleRate());
  
  // set the sensitivity to 300 milliseconds
  // After a beat has been detected, the algorithm will wait for 300 milliseconds 
  // before allowing another beat to be reported. You can use this to dampen the 
  // algorithm if it is giving too many false-positives. The default value is 10, 
  // which is essentially no damping. If you try to set the sensitivity to a negative value, 
  // an error will be reported and it will be set to 10 instead. 
  beat.setSensitivity(BEAT_SENSITIVITY);
  
  // make a new beat listener, so that we won't miss any buffers for the analysis
  bl = new BeatListener(beat, source);
}

void setupSerial()
{
  if(Serial.list().length > 0)
  {
    port = new Serial(this, Serial.list()[0], SERIAL_SPEED);
  }
  else
  {
    println("Arduino not connected, disabling serial output.");
    arduino_connected = false;
  }
}

void sendSerial(int data)
{
  if(arduino_connected)
  {
    port.write(data); 
  }
}

void playSong()
{
  if(song != null)
  {
    if(repeat)
    {
      song.loop();
    }
    else
    {
      song.play();
    }
  }
}

void closeSong()
{
 if(song != null)
  {
    song.close();
  } 
}

void closeStream()
{
  if(input != null)
  {
    input.close();
  }  
}

boolean songFinished()
{
  return !song.isPlaying() && song.position() >= song.length()*0.98;
}

boolean songPlaying()
{
  return song != null && song.isPlaying();
}

int songPos()
{
  if(song == null)
  {
    return 0;
  }
  return song.position();
}

int songLength()
{
  if(song == null)
  {
    return 0;
  }
  return song.length();
}

String formatTime(int time)
{
 int min, sec;

 min = time / 60000;
 sec = (time % 60000) / 1000;
 
 return min + ":" + twoDigit(sec);
}

String twoDigit(int n)
{
  if(n > 9)
  {
    return "" + n;
  }
  else
  {
    return "0" + n;
  }
}
