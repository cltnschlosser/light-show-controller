//Christmas lights

const int LIGHTS = 12;
const int LAMP = 8;
const int LIGHTS2 = 7;

const int NUM_CHANNELS = 3;

const unsigned long SERIAL_RATE = 57600;

unsigned long pinTime[NUM_CHANNELS];
const int pinMap[] = {LIGHTS, LAMP, LIGHTS2};
const int timeMap[] = {150, 300, 200};//Set the time each channel will pulse for

void setup() {
  for(int i = 0; i < NUM_CHANNELS; i++)
  {
    pinMode(pinMap[i], OUTPUT);
  }
  
  Serial.begin(SERIAL_RATE);
}

void loop()
{ 
  serial();
  
  processPins();
}

void serial()
{
 if(Serial.available())
 {
   int pin = Serial.read();
   pulsePin(pin);
 }
}

void pulsePin(int pin)
{
  for(int i = 0; i < NUM_CHANNELS; i++)
  {
    if(pin == pinMap[i])
    {
      pinTime[i] = millis() + timeMap[i];
      return;
    }
  }
}

void processPins()
{
 for(int i = 0; i < NUM_CHANNELS; i++)
 {
  digitalWrite(pinMap[i], pinTime[i] > millis());
 }
}
