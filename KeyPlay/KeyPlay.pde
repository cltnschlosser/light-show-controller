import processing.serial.*;
import ddf.minim.*;
import ddf.minim.analysis.*;

int ledPin =  12;
int ledPin2 =  8;
int ledPin3 =  7;

float kickSize, snareSize, hatSize;

Serial port;

void setup() {
  size(512, 200, P3D);
  textFont(createFont("Helvetica", 16));
  textAlign(CENTER);
  
  kickSize = snareSize = hatSize = 16;

  port = new Serial(this, Serial.list()[0], 57600);
}

void keyPressed()
{
  if (keyCode == RIGHT)
  {
    port.write(ledPin);
    hatSize = 32;
  }
  if (keyCode == DOWN)
  {
    port.write(ledPin2);
    snareSize = 32;
  }
  if(keyCode == LEFT)
  {
    port.write(ledPin3);
    kickSize = 32;
  }
}

void draw() {
  background(0);
  fill(255);
  
  textSize(kickSize);
  text("KICK", width/4, height/2);
  
  textSize(snareSize);
  text("SNARE", width/2, height/2);
  
  textSize(hatSize);
  text("HAT", 3*width/4, height/2);
  
  kickSize = constrain(kickSize * 0.95, 16, 32);
  snareSize = constrain(snareSize * 0.95, 16, 32);
  hatSize = constrain(hatSize * 0.95, 16, 32);
}

void stop() {
  super.stop();
}
